## Express Routes

* How to create a router
* How to mount a router at a path
* Best practices for URLs

Watch the Video [here](https://www.youtube.com/watch?v=xEDpRbJtlKA)
