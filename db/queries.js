const dinosaurs = [
  {
    id: 0,
    name: 'T-Rex'
  }, {
    id: 1,
    name: 'Velociraptor'
  }, {
    id: 2,
    name: 'Stegasaurus'
  }
];

function find(id) {
  return dinosaurs.filter(d => d.id == id)[0];
}

module.exports = {
  getAll() {
    return Promise.resolve(dinosaurs);
  },
  getOne(id) {
    const found = find(id);
    return Promise.resolve(found);
  },
  create(dinosaur) {
    dinosaur.id = dinosaurs[length-1].id + 1;
    dinosaurs.push(dinosaur);
    return Promise.resolve(dinosaur);
  },
  update(id, dinosaur) {
    const found = find(id);
    found.name = dinosaur.name;
    return Promise.resolve(found);
  },
  delete(id) {
    const found = find(id);
    const index = dinosaurs.indexOf(found);
    dinosaurs.splice(index, 1);
    return Promise.resolve();
  }
};
