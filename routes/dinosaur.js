const express = require('express');
const dinosaurRouter = express.Router();

const queries = require('../db/queries');

/* All routes in this file will be prepended with the path /dinosaur */

// READ - read all
dinosaurRouter.get('/', (req, res) => {
  queries
    .getAll()
    .then(all => {
      res.render('all', {dinosaurs: all});
    });
});

// READ - read one
dinosaurRouter.get('/:id', (req, res) => {
  queries
    .getOne(req.params.id)
    .then(one => {
      res.render('single', {
        dinosaur: one
      });
    });
});

// Update - show update form
dinosaurRouter.get('/:id/edit', (req, res) => {
  queries
    .getOne(req.params.id)
    .then(one => {
      // doesn't exist yet!
      res.render('edit', {
        dinosaur: one
      });
    });
});

// CREATE - creates one
dinosaurRouter.post('/', (req, res) => {
  queries
    .create(req.params.id, req.body)
    .then(dinosaur => {
      res.redirect(`/dinosaur/${dinosaur.id}`);
    });
});

// UPDATE - updates one
dinosaurRouter.put('/:id', (req, res) => {
  queries
    .update(req.params.id, req.body)
    .then(dinosaur => {
      res.redirect(`/dinosaur/${dinosaur.id}`);
    });
});

// DELETE - deletes one
dinosaurRouter.delete('/:id', (req, res) => {
  queries
    .delete(req.params.id)
    .then(() => {
      res.redirect('/dinosaur');
    });
});

module.exports = dinosaurRouter;
